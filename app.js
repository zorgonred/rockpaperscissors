let userScore = 0;
let computerScore= 0;
const userScore_span = document.getElementById('user-score');
const computerScore_span = document.getElementById('computer-score');
const result_p = document.querySelector('.result > p');
const rock_div = document.getElementById('rock');
const paper_div = document.getElementById('paper');
const scissors_div = document.getElementById('scissors');

//This is the function where the users choice is communicated to the game. Next I will make teh Game
//function
function main() {
  rock_div.addEventListener('click', function() {
    game('rock');
  });

  paper_div.addEventListener('click', function() {
    game('paper');
  });

  scissors_div.addEventListener('click', function() {
    game('scissors');
  });
}//main


main();

//I also need to make the computers choice which will be brought into this function
function game(userChoice){
  const computerChoice = getComputerChoice();
  switch (userChoice+computerChoice) {
    case 'rockscissors':
    case 'paperrock':
    case 'scissorspaper':
    win(userChoice,computerChoice)
    //instead of printing to console i need to make it print on the html page
    // console.log('win');
      break;
    case 'rockpaper':
    case 'rockscissors':
    case 'paperscissors':
    lose(userChoice,computerChoice)
    //instead of printing to console i need to make it print on the html page
    // console.log('lose');
      break;
    case 'rockrock':
    case 'paperpaper':
    case 'scissorsscissors':
    draw(userChoice,computerChoice)
    //instead of printing to console i need to make it print on the html page
    // console.log('draw');
    break;


  }//switch

}//game


function getComputerChoice(){
  const choices = ['rock','paper','scissors']
  //this will return 3 indexes , which will randomly select 0, 1, 2 [rock,paper,scissors]
  const randomChoice = Math.floor(Math.random() * 3);
  return choices[randomChoice];

}//getComputerChoice


function win(userChoice,computerChoice)
{
//count
userScore++;
computerScore++;
userScore_span.innerHTML = userChoice;
computerScore_span.innerHTML = computerChoice;
result_p.innerHTML = userChoice +' beats ' +computerChoice;

}//win


function lose(userChoice,computerChoice)
{
//count
userScore++;
computerScore++;
userScore_span.innerHTML = userChoice;
computerScore_span.innerHTML = computerChoice;
result_p.innerHTML = userChoice +' loses to ' +computerChoice;

}//lose

function draw(userChoice,computerChoice)
{
//count
userScore++;
computerScore++;
userScore_span.innerHTML = userChoice;
computerScore_span.innerHTML = computerChoice;
result_p.innerHTML = userChoice +' draws to ' +computerChoice;

}//draw
